# elastic-poc

`gitlab-internal` gcp project

create `mwas-test` network in `gitlab-internal` (I spent 0.5h trying to create a cluster in the default networks and failed to figure out how to do it)

create a cluster: `gcloud container clusters create mwas-test --preemptible --enable-autoscaling --max-nodes=10 --min-nodes=1 --zone=europe-west3-a --network=mwas-test --subnetwork=mwas-test`

create a service account for tiller: `kubectl apply -f helm.yaml`

initialize helm: `helm init --service-account helm`

run helmfile: `helmfile apply`

access Kibana using: `kubectl -n observability port-forward service/kibana-kibana 5601`
